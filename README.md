# Shop Laravel

## Getting Started

**_Install NodeJS_**

```
https://nodejs.org/en/download/
```

**_Install XAMPP_**

```
https://www.apachefriends.org/index.html
```

**_Install Composer_**

```
https://getcomposer.org/
```

**_Create account AWS_** create S3 bucket, IAM user

```
https://aws.amazon.com/vi/
```

**_Create account pusher_**

```
https://pusher.com/
```

## Installation

A step by step series of examples that tell you how to get a development env running
Clone project

```
git clone https://gitlab.com/tinnguyen2597/shop_laravel.git
```

Then

```
cd shop_laravel
```

Open vscode or ....
`Copy file .env.example and change name .env`
**Set up transmission value variables in .env**
Then
open terminal

```
npm install -g npm
npm install -g yarn
npm install
yarn install
composer install
php artisan key:generate
yarn run dev or npm run dev
```

if not file package.json
`npm install`
then copy

```
"dev": "npm run development",
"development": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
"watch": "npm run development -- --watch",
"watch-poll": "npm run watch -- --watch-poll",
"hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
"prod": "npm run production",
"production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
```

to **scripts** and copy

```
"axios": "^0.18",
"bootstrap": "^4.1.0",
"cross-env": "^5.1",
"jquery": "^3.4.1",
"laravel-mix": "^4.0.7",
"lodash": "^4.17.5",
"popper.js": "^1.12",
"resolve-url-loader": "^2.3.1",
"sass": "^1.15.2",
"sass-loader": "^7.1.0",
"vue": "^2.5.17"
```

to **devDependencies** and follow the steps above

## Authors

-   NTT

## File

-   app `handle controller, view. log, etc`
-   bootstrap
-   config
-   database
-   db _backup database_
-   public
-   resources ** layout, css, js, sass, etc **
-   routes ** url pattern as /trang-chu **
-   storage
-   tests ** test case and unit test **
-   verdor ** css, js of template **
